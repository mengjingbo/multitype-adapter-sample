package com.multitype.adapter.sample

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.multitype.adapter.MultiTypeAdapter
import com.multitype.adapter.binder.MultiTypeBinder
import com.multitype.adapter.callback.OnViewClickListener
import com.multitype.adapter.createMultiTypeAdapter
import com.multitype.adapter.sample.binder.*
import com.multitype.adapter.sample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OnViewClickListener {

    private lateinit var mAdapter: MultiTypeAdapter
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.setVariable(BR.data, this)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        mAdapter = createMultiTypeAdapter(binding.multiTypeRecycler, LinearLayoutManager(this))
        setRecyclerViewContent()
    }

    private fun setRecyclerViewContent() {
        mAdapter.notifyAdapterChanged(mutableListOf<MultiTypeBinder<*>>().apply {
            add(TopBannerBinder())
            add(CategoryContainerBinder(listOf("男装", "女装", "鞋靴", "内衣内饰", "箱包", "美妆护肤", "洗护", "腕表珠宝", "手机", "数码").map {
                CategoryItemBinder(it)
            }))
            add(RecommendContainerBinder((1..8).map { RecommendGoodsBinder() }))
            add(HorizontalScrollBinder((0..11).map { it }))
            add(GoodsGridContainerBinder((1..20).map { GoodsBinder() }))
        })
    }

    override fun onClick(view: View, any: Any?) {
//        if (view.id == R.id.multi_type_item_text) {
//            Log.e(this.javaClass.simpleName, "${any}被点击")
//        }
    }
}
